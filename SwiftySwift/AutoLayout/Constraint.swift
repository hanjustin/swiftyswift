
import UIKit

typealias LayoutAnchor<T: AnyObject> = (leftAnchor: NSLayoutAnchor<T>, relation: NSLayoutRelation, rightAnchor: NSLayoutAnchor<T>)

typealias LayoutDimension = (leftDimension: NSLayoutDimension, relation: NSLayoutRelation, rightDimension: NSLayoutDimension)

typealias LayoutConstantDimension = (leftDimension: NSLayoutDimension, relation: NSLayoutRelation, constant: CGFloat)

func ==<T: AnyObject>(lhs: NSLayoutAnchor<T>, rhs: NSLayoutAnchor<T>) -> LayoutAnchor<T> {
    return (leftAnchor: lhs, relation: .equal, rightAnchor: rhs)
}

func >=<T: AnyObject>(lhs: NSLayoutAnchor<T>, rhs: NSLayoutAnchor<T>) -> LayoutAnchor<T> {
    return (leftAnchor: lhs, relation: .greaterThanOrEqual, rightAnchor: rhs)
}

func <=<T: AnyObject>(lhs: NSLayoutAnchor<T>, rhs: NSLayoutAnchor<T>) -> LayoutAnchor<T> {
    return (leftAnchor: lhs, relation: .lessThanOrEqual, rightAnchor: rhs)
}

func ==(lhs: NSLayoutDimension, constant: CGFloat) -> LayoutConstantDimension {
    return (leftDimension: lhs, relation: .equal, constant: constant)
}

func >=(lhs: NSLayoutDimension, constant: CGFloat) -> LayoutConstantDimension {
    return (leftDimension: lhs, relation: .greaterThanOrEqual, constant: constant)
}

func <=(lhs: NSLayoutDimension, constant: CGFloat) -> LayoutConstantDimension {
    return (leftDimension: lhs, relation: .lessThanOrEqual, constant: constant)
}

func ==(constant: CGFloat, dimension: NSLayoutDimension) -> LayoutConstantDimension {
    return (leftDimension: dimension, relation: .equal, constant: constant)
}

func >=(constant: CGFloat, dimension: NSLayoutDimension) -> LayoutConstantDimension {
    return (leftDimension: dimension, relation: .greaterThanOrEqual, constant: constant)
}

func <=(constant: CGFloat, dimension: NSLayoutDimension) -> LayoutConstantDimension {
    return (leftDimension: dimension, relation: .lessThanOrEqual, constant: constant)
}

func ==(lhs: NSLayoutDimension, rhs: NSLayoutDimension) -> LayoutDimension {
    return (leftDimension: lhs, relation: .equal, rightDimension: rhs)
}

func >=(lhs: NSLayoutDimension, rhs: NSLayoutDimension) -> LayoutDimension {
    return (leftDimension: lhs, relation: .greaterThanOrEqual, rightDimension: rhs)
}

func <=(lhs: NSLayoutDimension, rhs: NSLayoutDimension) -> LayoutDimension {
    return (leftDimension: lhs, relation: .lessThanOrEqual, rightDimension: rhs)
}

// MARK: - Constraint

class Constraint {
    @discardableResult
    static func set<T>(
        _ layout: LayoutAnchor<T>,
        constant: CGFloat = 0,
        priority: UILayoutPriority = UILayoutPriorityRequired)
        -> NSLayoutConstraint
    {
        return set(
            lhs: layout.leftAnchor,
            relation: layout.relation,
            rhs: layout.rightAnchor,
            constant: constant,
            priority: priority)
    }
    
    @discardableResult
    static func set(
        _ layout: LayoutDimension,
        multiplier: CGFloat = 1,
        constant: CGFloat = 0,
        priority: UILayoutPriority = UILayoutPriorityRequired)
        -> NSLayoutConstraint
    {
        return set(
            lhs: layout.leftDimension,
            relation: layout.relation,
            rhs: layout.rightDimension,
            multiplier: multiplier,
            constant: constant,
            priority: priority)
    }
    
    @discardableResult
    static func set(
        _ layout: LayoutConstantDimension,
        priority: UILayoutPriority = UILayoutPriorityRequired)
        -> NSLayoutConstraint
    {
        return set(lhs: layout.leftDimension, relation: layout.relation, rhs: nil, constant: layout.constant, priority: priority)
    }
}

private extension Constraint {
    static func set(lhs: Any, relation: NSLayoutRelation, rhs: Any?, multiplier: CGFloat = 1, constant: CGFloat = 0, priority: UILayoutPriority) ->  NSLayoutConstraint {
        let constraint: NSLayoutConstraint
        
        switch (lhs, rhs) {
        case (let lhs as NSLayoutDimension, let rhs as NSLayoutDimension):
            constraint = createConstraintWith(lhs: lhs, relation: relation, rhs: rhs, multiplier: multiplier, constant: constant)
        case (let dimension as NSLayoutDimension, nil):
            constraint = createConstraintWith(dimension: dimension, relation: relation, constant: constant)
        case (let lhs as NSLayoutAnchor<AnyObject>, let rhs as NSLayoutAnchor<AnyObject>):
            constraint = createConstraintWith(lhs: lhs, relation: relation, rhs: rhs, constant: constant)
        default:
            fatalError("Invalid NSLayoutConstraint creation attempt")
        }
        
        constraint.priority = priority
        constraint.isActive = true
        return constraint
    }
    
    static func createConstraintWith(
        lhs: NSLayoutAnchor<AnyObject>,
        relation: NSLayoutRelation,
        rhs: NSLayoutAnchor<AnyObject>,
        constant: CGFloat)
        -> NSLayoutConstraint
    {
        switch relation {
        case .equal:
            return lhs.constraint(equalTo: rhs, constant: constant)
        case .greaterThanOrEqual:
            return lhs.constraint(greaterThanOrEqualTo: rhs, constant: constant)
        case .lessThanOrEqual:
            return lhs.constraint(lessThanOrEqualTo: rhs, constant: constant)
        }
    }
    
    static func createConstraintWith(
        dimension: NSLayoutDimension,
        relation: NSLayoutRelation,
        constant: CGFloat)
        -> NSLayoutConstraint
    {
        switch relation {
        case .equal:
            return dimension.constraint(equalToConstant: constant)
        case .greaterThanOrEqual:
            return dimension.constraint(greaterThanOrEqualToConstant: constant)
        case .lessThanOrEqual:
            return dimension.constraint(lessThanOrEqualToConstant: constant)
        }
    }
    
    static func createConstraintWith(
        lhs: NSLayoutDimension,
        relation: NSLayoutRelation,
        rhs: NSLayoutDimension,
        multiplier: CGFloat,
        constant: CGFloat)
        -> NSLayoutConstraint
    {
        switch relation {
        case .equal:
            return lhs.constraint(equalTo: rhs, multiplier: multiplier, constant: constant)
        case .greaterThanOrEqual:
            return lhs.constraint(greaterThanOrEqualTo: rhs, multiplier: multiplier, constant: constant)
        case .lessThanOrEqual:
            return lhs.constraint(lessThanOrEqualTo: rhs, multiplier: multiplier, constant: constant)
        }
    }
}
