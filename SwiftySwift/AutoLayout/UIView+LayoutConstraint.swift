
import UIKit

extension NSLayoutConstraint {
    func on() {
        isActive = true
    }
    
    func off() {
        isActive = false
    }
}

public extension UIView {
    @discardableResult
    func constraint(
        _ attribute: NSLayoutAttribute,
        relatedBy relation: NSLayoutRelation,
        to targetAttribute: NSLayoutAttribute,
        ofItem item: Any?,
        multipler: CGFloat = 1,
        constant: CGFloat = 0,
        priority: UILayoutPriority = 1000)
        -> NSLayoutConstraint
    {
        let constraint = NSLayoutConstraint(
            item: self,
            attribute: attribute,
            relatedBy: relation,
            toItem: item,
            attribute: targetAttribute,
            multiplier: multipler,
            constant: constant)
        
        constraint.priority = priority
        constraint.isActive = true
        return constraint
    }
    
//    @discardableResult
//    func widthConstraint(_ relation: NSLayoutRelation, to width: CGFloat) -> NSLayoutConstraint {
//        let constraint = NSLayoutConstraint(
//            item: self,
//            attribute: .width,
//            relatedBy: relation,
//            toItem: nil,
//            attribute: .notAnAttribute,
//            multiplier: 1.0,
//            constant: width)
//        
//        constraint.isActive = true
//        return constraint
//    }
    
    
    
    
    
    func pinEdges(_ edges: Edges = [.leadingTrailing, .topBottom], to view: UIView) {
        if edges.contains(.leading) {
            leadingAnchor.constraint(equalTo: view.leadingAnchor).on()
        }
        if edges.contains(.trailing) {
            trailingAnchor.constraint(equalTo: view.trailingAnchor).on()
        }
        if edges.contains(.top) {
            topAnchor.constraint(equalTo: view.topAnchor).on()
        }
        if edges.contains(.bottom) {
            bottomAnchor.constraint(equalTo: view.bottomAnchor).on()
        }
    }
    
    func equalCenter(to view: UIView) {
        centerXAnchor.constraint(equalTo: view.centerXAnchor).on()
        centerYAnchor.constraint(equalTo: view.centerYAnchor).on()
    }
}
