//
//  UILabel+TextFormatting.swift
//  SwiftySwift
//
//  Created by MedCircle on 2/16/17.
//  Copyright © 2017 hanjustin. All rights reserved.
//

import Foundation

extension UILabel {
    public func set(font newFont: UIFont, to string: String) {
        add(attribute: NSFontAttributeName, value: newFont, to: range(of: string))
    }
    
    public func set(color: UIColor, to string: String) {
        add(attribute: NSForegroundColorAttributeName, value: color, to: range(of: string))
    }
}

private extension UILabel {
    // MARK: - Range functions
    
    func range(of string: String) -> NSRange? {
        let range = NSString(string: text ?? "").range(of: string)
        return range.location == NSNotFound ? nil : range
    }
    
    // MARK: - Helper
    
    func mutableAttributedText() -> NSMutableAttributedString {
        return attributedText.unwrap(NSMutableAttributedString.init(attributedString:)) ?? NSMutableAttributedString(string: text ?? "")
    }
    
    func add(attribute: String, value: Any, to range: NSRange?) {
        guard let range = range else { return }
        let newAttributedText = mutableAttributedText()
        newAttributedText.addAttribute(attribute, value: value, range: range)
        attributedText = newAttributedText
    }
}
