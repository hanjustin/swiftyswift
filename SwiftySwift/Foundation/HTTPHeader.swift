
import Foundation

public enum HTTPHeader {
    case authorization(type: String, token: String?)
    case content(type: ContentType)
    case custom(key: String, value: String?)
    
    public enum ContentType: String {
        case json = "application/json"
        case urlEncoded = "application/x-www-form-urlencoded"
        case multiPartFormData = "multipart/form-data"
    }
}

extension HTTPHeader {
    public var key: String { return kvp.key }
    public var value: String? { return kvp.value }
}

private extension HTTPHeader {
    var kvp: (key: String, value: String?) {
        switch self {
        case .authorization(let type, let token):
            return ("Authorization", type + " " + (token ?? ""))
        case .content(let contentType):
            return ("Content-Type", contentType.rawValue)
        case .custom(let key, let value):
            return (key, value)
        }
    }
}
