
import UIKit

extension UIColor {
    public convenience init(r: Int, g: Int, b: Int, alpha: Double = 1.0) {
        self.init(red: CGFloat(r)/255, green: CGFloat(g)/255, blue: CGFloat(b)/255, alpha: CGFloat(alpha))
    }
}
