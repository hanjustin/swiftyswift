//
//  UINavigationBar.swift
//  SwiftySwift
//
//  Created by MedCircle on 2/13/17.
//  Copyright © 2017 hanjustin. All rights reserved.
//

import Foundation

extension UINavigationBar {
    public func setTitleColorAs(_ color: UIColor) {
        var attributes = titleTextAttributes ?? [:]
        attributes[NSForegroundColorAttributeName] = color
        titleTextAttributes = attributes
    }
}
