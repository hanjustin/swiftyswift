
import Foundation

public protocol PossiblyEmpty {
    var nonEmptyValue: Self { get }
    var isEmpty: Bool { get }
}

// MARK: - RangeReplaceableCollection

extension Optional {
    public var isNil: Bool {
        switch self {
        case .none: return true
        case .some(_): return false
        }
    }
    
    public var isNotNil: Bool { return isNil.isFalse }
}

extension Optional where Wrapped: RangeReplaceableCollection {
    public mutating func appendOrInitWith<S : Sequence>(contentsOf newElements: S) where S.Iterator.Element == Wrapped.Iterator.Element {
        switch self {
        case .none:
            self = .some(Wrapped.init(newElements))
        case .some(let value):
            var copy = value
            copy.append(contentsOf: newElements)
            self = .some(copy)
        }
    }
}
