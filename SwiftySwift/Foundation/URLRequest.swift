
import Foundation

public protocol URLConvertible {
    func asURL() throws -> URL
}

extension URL: URLConvertible {
    public func asURL() throws -> URL { return self }
    
    init?(string: String?) {
        guard let string = string else { return nil }
        self.init(string: string)
    }
}

extension String: URLConvertible {
    public func asURL() throws -> URL {
        guard let url = URL(string: self) else { throw NetworkError.invalidURL(url: self) }
        return url
    }
}

extension URL {
    public mutating func appendQueryStrings(with parameters: JSON) {
        var components = URLComponents(url: self, resolvingAgainstBaseURL: false)
        var newQueryItems = [URLQueryItem]()
        
        parameters.forEach { key, value in
            if let array = value as? [Any] {
                let commaSplitParams = array.map { ($0 as? String) ?? (String(describing: $0)) }.joined(separator: ",")
                newQueryItems.append(URLQueryItem(name: key, value: commaSplitParams))
            } else {
                newQueryItems.append(URLQueryItem(name: key, value: (value as? String) ?? (String(describing: value))))
            }
        }
        
        components?.queryItems.appendOrInitWith(contentsOf: newQueryItems)
        self = components?.url ?? self
    }
}
