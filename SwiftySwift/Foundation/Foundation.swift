
import Foundation

// MARK: - Bool

extension Bool {
    public var isFalse: Bool { return self == false }
}

// MARK: - String

extension String {
    public var isNotWhiteSpace: Bool { return trimmingCharacters(in: .whitespaces).isEmpty.isFalse }
    public var nonEmptyValue: String? { return isNotWhiteSpace ? self : nil }
    public var length: Int { return characters.count }
    
    public func matchesRegex(pattern: String) -> Bool {
        let predicate = NSPredicate(format: "SELF MATCHES %@", pattern)
        return predicate.evaluate(with: self)
    }
}

// MARK: - Array

extension Array {
    public subscript(safe index: Int) -> Element? {
        get { return 0..<count ~= index ? self[index] : nil }
    }
}

public protocol ArrayProtocol {
    associatedtype Element
    var count: Int { get }
    
    func toArray() -> [Element]
    mutating func append(_ newElement: Element)
    mutating func append(contentsOf: [Element])
}

extension Array: ArrayProtocol {
    public func toArray() -> [Element] {
        return self
    }
}

// MARK: - OptionSet
// Reference: http://stackoverflow.com/questions/32102936/how-do-you-enumerate-optionsettype-in-swift
extension OptionSet where RawValue : Integer {
    public func elements() -> AnySequence<Self> {
        var remainingBits = rawValue
        var bitMask: RawValue = 1
        
        return AnySequence {
            return AnyIterator {
                while remainingBits != 0 {
                    defer { bitMask = bitMask &* 2 }
                    if remainingBits & bitMask != 0 {
                        remainingBits = remainingBits & ~bitMask
                        return Self(rawValue: bitMask)
                    }
                }
                return nil
            }
        }
    }
}

extension Dictionary {
    public mutating func rename(_ key: Key, to newKey: Key) {
        self[newKey] = self[key]
        self[key] = nil
    }
}

public func +<T>(lhs: Array<T>, rhs: Array<T>) -> Array<T> {
    return [lhs, rhs].flatMap { $0 }
}

public func +(lhs: CGSize, rhs: CGSize) -> CGSize {
    return CGSize(width: lhs.width + rhs.width, height: lhs.height + rhs.height)
}

//extension JSONSerialization {
//    static func jsonObject(with data: Data, ddd: ReadingOptions) throws {
//        guard let json = try jsonObject(with: data, options: ddd) else { throw JSONSerialization
//        
//    }
//}
