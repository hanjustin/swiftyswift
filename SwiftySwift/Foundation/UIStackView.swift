
import UIKit

extension UIStackView {
    public func addArrangedSubviews(views: [UIView]) {
        for view in views {
            addArrangedSubview(view)
        }
    }
}
