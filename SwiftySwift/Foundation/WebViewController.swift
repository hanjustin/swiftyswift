
import UIKit
import WebKit

open class WebViewController: UIViewController {
    let url: URL
    
    let webView = WKWebView(frame: .zero)
    
    public init(url: URL) {
        self.url = url
        super.init(nibName: nil, bundle: nil)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(webView, autoResizeMaskEnabled: false)
        view.edges |=| webView
        webView.load(URLRequest(url: url))
    }
    
    
    
}
