
import Foundation

public protocol DescriptiveError: Error {
    var errorDescription: String { get }
}

// MARK: - NetworkError

public enum NetworkError {
    case unauthorized
    case invalidURL(url: URLConvertible)
    case jsonEncodingFailed(error: Error)
    case serverErrorResponse(message: String)
    case custom(message: String)
}

extension NetworkError: DescriptiveError {
    public var errorDescription: String {
        switch self {
            case .unauthorized:
                return "Unauthorized. Invalid access."
            case .invalidURL(let url):
                return "URL is not valid: \(url)"
            case .jsonEncodingFailed(let error):
                return "JSON could not be encoded because of error:\n\(error.localizedDescription)"
            case .serverErrorResponse(let message), .custom(let message):
                return message
        }
    }
}

public enum Result<T> {
    case success(T)
    case failure(Error)
    
    public func genericVersion() -> Result<Void> {
        switch self {
        case .failure(let error):
            return .failure(error)
        case .success:
            return .success(Void())
        }
    }
    
    public var isSuccess: Bool {
        switch self {
        case .success: return true
        case .failure: return false
        }
    }
    
    public var isFailure: Bool {
        return isSuccess.isFalse
    }
}

//enum RequestError: Error {
//    enum ParameterEncodingFailureReason {
//        case requestCreationFailed
//    }
//    
//    enum ResponseValidationFailureReason {
//        case dataFileNil
//    }
//    
//    enum ResponseSerializationFailureReason {
//        case jsonTypeCastFailed
//        case jsonSerializationFailed
//    }
//    
//    case parameterEncodingFailed(reason: ParameterEncodingFailureReason)
//    case responseValidationFailed(reason: ResponseValidationFailureReason)
//    case responseSerializationFailed(reason: ResponseSerializationFailureReason)
//}
