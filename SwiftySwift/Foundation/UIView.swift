
import UIKit

extension UIView {
    public var cornerRadius: CGFloat {
        get { return layer.cornerRadius }
        set { layer.cornerRadius = newValue; layer.masksToBounds = newValue > 0 }
    }
    
    public func setBorder(edges: Edges, color: UIColor = .black, thickness: CGFloat = 10) {
        for edge in edges.elements() {
            let border = UIView().with { $0.backgroundColor = color }
            let isLeftRightBorder = [.leading, .trailing].contains(edge)
            //            let borderSize: [LayoutDataConvertible] = isLeftRightBorder ? [thickness, self] : [thickness, self]
            let borderCenter = isLeftRightBorder ? centerY : centerX
            self |+| border
            border.size |=| (isLeftRightBorder ? (thickness, self) : (self, thickness))
            border |=| [borderCenter, constraint(of: edge)]
        }
    }
    // Rename this to addSubview(view, autoResizeConstraints enabled: Bool)
    public func addSubviewWithMaskConstraintsDisabled(_ view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
    }
    
    public func addSubview(_ view: UIView, autoResizeMaskEnabled enabled: Bool) {
        view.translatesAutoresizingMaskIntoConstraints = enabled
        addSubview(view)
    }
    
    public var parentViewController: UIViewController? {
        var currentResponder: UIResponder? = self
        while let responder = currentResponder {
            if let vc = responder as? UIViewController { return vc }
            currentResponder = currentResponder?.next
        }
        return nil
    }
}

extension UIViewController {
    public func presentAlertWith(title: String?, message: String? = nil, handler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: handler)
        
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
    
    public func presentAlert(for error: Error, handler: ((UIAlertAction) -> Void)? = nil) {
        switch error {
        case let error as DescriptiveError:
            presentAlertWith(title: error.errorDescription, handler: handler)
        default:
            presentAlertWith(title: error.localizedDescription, message: String(describing: error), handler: handler)
        }
        
        
    }
}
