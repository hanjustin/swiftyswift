//
//  SwiftyButton.swift
//  SwiftySwift
//
//  Created by MedCircle on 3/1/17.
//  Copyright © 2017 hanjustin. All rights reserved.
//

import Foundation

open class SwiftyButton: UIButton {
    open var extraHitArea: UIEdgeInsets? // For increasing  area, the inset should be negative.
    
    override open func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        guard let extraHitArea = extraHitArea else { return super.point(inside: point, with: event) }
        let areaWithExtraHitPadding = UIEdgeInsetsInsetRect(bounds, extraHitArea)
        return areaWithExtraHitPadding.contains(point)
    }
}
