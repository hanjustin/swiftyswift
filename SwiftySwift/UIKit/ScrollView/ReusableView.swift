
import Foundation

public protocol ReusableView: class {}

extension ReusableView where Self: UIView {
    public static var reuseIdentifier: String {
        return String(describing: self)
    }
}

public protocol ReusableViewPresantable {
    associatedtype reusableView: ReusableView
}


extension UITableViewCell: ReusableView { }

// MARK: - UITableView

extension UITableView {
    public func register<ReusableCell: UITableViewCell>(_: ReusableCell.Type) where ReusableCell: ReusableView {
        register(ReusableCell.self, forCellReuseIdentifier: ReusableCell.reuseIdentifier)
    }
    
    public func dequeueReusableCell<ReusableCell: ReusableView>(forIndexPath indexPath: IndexPath) -> ReusableCell where ReusableCell: UITableViewCell {
        guard let cell = dequeueReusableCell(withIdentifier: ReusableCell.reuseIdentifier, for: indexPath) as? ReusableCell else {
            fatalError("Could not dequeue cell with identifier: \(ReusableCell.reuseIdentifier)")
        }
        
        return cell
    }
    
    public func dequeue<ReusableCell: ReusableView>(_ reusableCell: ReusableCell.Type) -> ReusableCell where ReusableCell: UITableViewCell {
        guard let cell = dequeueReusableCell(withIdentifier: ReusableCell.reuseIdentifier) as? ReusableCell else {
            fatalError("Could not dequeue cell with identifier: \(ReusableCell.reuseIdentifier)")
        }
        
        return cell
    }
    
//    func dequeueReusableCell<T: UITableViewCell>(forIndexPath indexPath: IndexPath) -> T where T: ReusableView {
//        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
//            fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier)")
//        }
//        return cell
//    }
    
//    func dequeueReusableCell<T: ReusableViewPresantable>(for item: T) -> T.reusableView where T.reusableView: UITableViewCell {
//        guard let cell = dequeueReusableCell(withIdentifier: T.reusableView.reuseIdentifier) as? T.reusableView else {
//            fatalError("Could not dequeue cell with identifier: \(T.reusableView.reuseIdentifier)")
//        }
//        return cell
//    }
}
