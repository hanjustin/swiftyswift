
import Foundation

open class ArrayDataSource<Item>: NSObject, UITableViewDataSource {
    public var sections: [SectionData<Item>] = [SectionData()]
//    public private(set) var filteredSections: [SectionData<Item>]?
    public var cellReuseIdentifierFinder: ((IndexPath) -> String)?
    public var cellConfigureBlock: ((UITableViewCell, Item) -> Void)?

    // MARK: - UITableViewDataSource
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let identifier = cellReuseIdentifierFinder?(indexPath) else { fatalError() }
        
        let item = sections[indexPath.section].items[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        cellConfigureBlock?(cell, item)
        
        return cell
    }
}
