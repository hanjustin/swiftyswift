
import Foundation

extension UITableView {
    public func hideEmptyCells() {
        tableFooterView = UIView()
    }
    
    public func indexPathFor(subView: UIView) -> IndexPath? {
        let viewOriginPosition = subView.convert(CGPoint.zero, to: self)
        return indexPathForRow(at: viewOriginPosition)
    }
}
