
import Foundation

open class AutoLayoutScrollView: UIScrollView {
    //    lazy private(set) var containerView: UIView = {
    //        let containerView = UIView()
    //        self.addSubviewWithMaskConstraintsDisabled(containerView)
    //        containerView.pinEdges(to: self)
    //        return containerView
    //    }()
    
    public enum ContentPosition {
        case top, centerY
    }
    
    public let containerView: UIView = UIView()
    
    public convenience init(contentView: UIView, contentPosition: ContentPosition = .centerY) {
        self.init()
        
        layout(contentView, at: contentPosition)
    }
    
    var disableHScroll = true {
        didSet {
            containerWidthConstraint.priority = (disableHScroll ? UILayoutPriorityRequired : 1)
        }
    }
    
    private var containerWidthConstraint: NSLayoutConstraint!
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        layoutContainerView()
        containerWidthConstraint = (width |=| containerView.width).constraints.first
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
//    override init() {
////        self.init()
//        
//        
//        AutoLayoutScrollView()
//        AutoLayoutScrollView(coder: <#T##NSCoder#>)
//    }
    
//    required public init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
}

private extension AutoLayoutScrollView {
    func layoutContainerView() {
        addSubview(containerView, autoResizeMaskEnabled: false)
        edges |=| containerView.edges
        height |=| containerView.height ! 1
    }
    
    func layout(_ contentView: UIView, at position: ContentPosition) {
        containerView.addSubview(contentView, autoResizeMaskEnabled: false)
        //        containerView.leading |<| contentView.leading
        //        containerView.trailing |>| contentView.trailing
        containerView.height |>| contentView.height
        containerView.width |>| contentView.width
        containerView.centerX |=| contentView.centerX
        
        switch position {
        case .top:
            containerView.top |=| contentView.top
        case .centerY:
            containerView.centerY |=| contentView.centerY
        }
    }
}

