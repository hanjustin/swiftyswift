
import UIKit

class AutoLayoutCellCollectionView<T>: UICollectionView, UICollectionViewDataSource {
    var myData = ["Short Text", "Looooonnnnnnnnnnnnnng Texxttttttt", "Medium Texxxtttt"]
    
    var sections = [SectionData<T>]()
    
    init(test: Int) {
        
        super.init(frame: .zero, collectionViewLayout: UICollectionViewLayout())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return UICollectionViewCell()
    }
}
