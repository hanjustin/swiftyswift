
import Foundation

public struct SectionData<T> {
    public var items: [T] = []
}
