
import Foundation

extension UIView {
    public func constraintEqualWidthAndHeight() {
        self.width |=| self.height
    }
}
