
import Foundation

open class SwiftyLabel: UILabel {
    open var textPadding: UIEdgeInsets = .zero
    open var expandToMaxWidthForMultiLine = true
    
    open override var text: String? {
        didSet {
            if expandToMaxWidthForMultiLine { preferredMaxLayoutWidth = 999999 }
        }
    }
    
    open override var attributedText: NSAttributedString? {
        didSet {
            if expandToMaxWidthForMultiLine { preferredMaxLayoutWidth = 999999 }
        }
    }
    
    open override func layoutSubviews() {
        preferredMaxLayoutWidth = bounds.size.width
        super.layoutSubviews()
    }
    
    override open func drawText(in rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, textPadding))
    }
    
    override open var intrinsicContentSize: CGSize {
        let verticalPadding = textPadding.top + textPadding.bottom
        let horizontalPadding = textPadding.left + textPadding.right
        return super.intrinsicContentSize + CGSize(width: horizontalPadding, height: verticalPadding)
    }
}
