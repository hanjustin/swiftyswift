
import Foundation

open class SwiftyImageView: UIImageView {
    
    open var isRoundImage = false
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        guard isRoundImage else { return }
        cornerRadius = bounds.width / 2
    }
    
}
