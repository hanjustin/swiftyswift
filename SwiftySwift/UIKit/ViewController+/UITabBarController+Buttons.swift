
import Foundation

extension UITabBarController {
    public var tabBarButtonViews: [UIView] {
        let interactionViews = tabBar.subviews.filter { $0.isUserInteractionEnabled }
        return interactionViews.sorted(by: {$0.frame.minX < $1.frame.minX})
    }
}
