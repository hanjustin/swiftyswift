
import Foundation

extension UIViewController {
    public var contentViewController: UIViewController {
        if let navVC = self as? UINavigationController {
            return navVC.topViewController?.contentViewController ?? self
        } else if let tabVC = self as? UITabBarController {
            return tabVC.selectedViewController?.contentViewController ?? self
        } else {
            return self
        }
    }
}
