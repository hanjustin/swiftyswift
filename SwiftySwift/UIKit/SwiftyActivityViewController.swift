//
//  SwiftyActivityViewController.swift
//  SwiftySwift
//
//  Created by MedCircle on 2/14/17.
//  Copyright © 2017 hanjustin. All rights reserved.
//

import Foundation

open class SwiftyActivityViewController: UIActivityViewController {
    public init(activityItems: [Any], customActivities: [UIActivity]? = nil, anchorView: UIView) {
        super.init(activityItems: activityItems, applicationActivities: customActivities)
        popoverPresentationController?.sourceView = anchorView.superview
        popoverPresentationController?.sourceRect = anchorView.frame
    }
    
    public init(activityItems: [Any], customActivities: [UIActivity]? = nil, anchorBarButtonItem: UIBarButtonItem) {
        super.init(activityItems: activityItems, applicationActivities: customActivities)
        popoverPresentationController?.barButtonItem = anchorBarButtonItem
    }
}
