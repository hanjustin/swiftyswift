
import Foundation

open class SwiftyStackView: UIStackView {
    open override var layoutMargins: UIEdgeInsets {
        didSet {
            if layoutMargins != .zero { isLayoutMarginsRelativeArrangement = true }
        }
    }
    
}
