
import UIKit

public protocol KeyboardTransitionViewAnimator {
    associatedtype contentViewType: UIView
    
    var contentView: contentViewType { get }
}

extension KeyboardTransitionViewAnimator where Self: UIViewController, contentViewType: UIScrollView {
    public func observeKeyboardTransitionNotifications() {
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillShow, object: nil, queue: .main, using: scrollUpContent)
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillHide, object: nil, queue: .main, using: scrollUpContent)
    }
    
    func scrollUpContent(notification: Notification) {
        guard
            let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height
            else { return }

        let moveUp = (notification.name == .UIKeyboardWillShow)
        let edgeInsets = moveUp ? UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0) : .zero
        contentView.contentInset = edgeInsets
        contentView.scrollIndicatorInsets = edgeInsets
    }
}
