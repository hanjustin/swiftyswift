
import Foundation

public protocol With { }

public extension With where Self: Any {
    func with(_ block: (inout Self) -> Void) -> Self {
        var copy = self
        block(&copy)
        return copy
    }
}

extension NSObject: With { }

//extension Optional where Wrapped: AnyObject {
//    public func newWith<T>(_ block: (Wrapped) -> T) -> T? {
//        switch self {
//        case .none: return nil
//        case .some(let value):
//            var copy = value
//            return block(copy)
//        }
//    }
//}

extension Optional {
    public func unwrap<T>(_ block: (Wrapped) throws -> T) rethrows -> T? {
        return try map(block)
    }
    
    public func flatUnwrap<T>(_ block: (Wrapped) throws -> T?) rethrows -> T? {
        return try flatMap(block)
    }
}





//extension With where Self: AnyObject {
//    func with(_ block: (Self) -> Void) -> Self {
//        block(self)
//        return self
//    }
//    
//    func `do`(_ block: (Self) -> Void) {
//        block(self)
//    }
//}
